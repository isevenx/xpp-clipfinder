#ifndef RECOGNIZER_H
#define RECOGNIZER_H

#include <QObject>
#include "smartdb.h"
#include "markupdb.h"
#include "streammodelloader.h"
#include "Struct/segmentcache.h"
#include "Struct/clipcache.h"
#include "Struct/cutcache.h"

#include "cutinserter.h"

class ClipFinderAlg;

namespace ClipFinder {

class Recognizer : public QObject
{
    Q_OBJECT
public:
    explicit Recognizer(SegmentCache * segmentCache, ClipCache * clipCache, CutCache * cutCache, SegmentCache * audioSegmentCache, ClipCache * audioClipCache, CutInserter * cutInserter, QObject *parent = 0);
    ~Recognizer();

signals:
    void allAdsLoaded();
    void segmentProcessed();
    void clipProcessed();
    void newClipProcessed();

public slots:
    void connectToDB();
    void loadAllAds();
    void process(qint64 segmentId);
    void rescanClip(qint64 modelId, qint64 minSegmentId);
    void newClip(qint64 modelId);

private:
    SmartDb * db;
    MarkupDB * markupDb;
    SegmentCache * segmentCache;
    ClipCache * clipCache;
    CutCache * cutCache;
    qint32 getFrameNumber(qint64 start, qint64 current, int frame, int fps);

    void check(bool &hasNoError, ClipFinderAlg * clipFinderAlg, SegmentCache::ModelInstance * streamInst, ClipCache::ClipInstance *clipModel, qint64 segmentId, qint64 modelId);
    void findClips(bool &hasNoError, ClipFinderAlg * clipFinderAlg, ClipCache::ClipInstance * clipModel, qint64 segmentId, qint64 modelId, SegmentCache::ModelInstance *streamInst);

    ClipCache::ClipInstance loadClip(qint64 modelId, bool &ok, ClipCache *clipCache, qint64 specificationId = -1, QString modelname = "cut_%1.model");

    std::vector<MarkupDB::TimeInfo> getRescanCutTimes(qint64 segmentId);

    ///for logging
    QString ipv4;
    QString ipv6;
    qint64 processId;
    qint64 totalFramesLog;

    ClipCache::ClipInstance clipModel;

    ///for audio
    SegmentCache *audioSegmentCache;
    ClipCache *audioClipCache;

    ///for clip saving
    CutInserter *cutInserter;
};

}

#endif // RECOGNIZER_H
