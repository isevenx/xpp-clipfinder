#include "dispatcher.h"

#include <QCommandLineParser>
#include <QDebug>
#include <QDir>
#include <QStandardPaths>
#include "smartopt.h"
#include "Opt/clipfinderopt.h"

namespace ClipFinder {

Dispatcher::Dispatcher(int &argc, char **argv):
    QCoreApplication(argc, argv),
    db(SmartOpt<ClipFinderOpt>::getInstance()->dbConfig),
    markupDb(&db),
    newSegmentQueue(SmartOpt<ClipFinderOpt>::getInstance()->newSegmentQueue,SmartQueue::ListeningQueue),
    learnedClipQueue(SmartOpt<ClipFinderOpt>::getInstance()->learnedClipQueue,SmartQueue::ListeningQueue),
    rescanClipQueue(SmartOpt<ClipFinderOpt>::getInstance()->rescanClipQueue,SmartQueue::ListeningQueue),
    rescanClipSendingQueue(SmartOpt<ClipFinderOpt>::getInstance()->rescanClipQueue,SmartQueue::SendingQueue),
    processedSegmentQueue(SmartOpt<ClipFinderOpt>::getInstance()->processedSegmentQueue,SmartQueue::SendingQueue),
    modelDeleteQueue(SmartOpt<ClipFinderOpt>::getInstance()->modelDeleteQueue,SmartQueue::ListeningQueue),
    absoluteSendingQueue(SmartOpt<ClipFinderOpt>::getInstance()->absoluteQueue,SmartQueue::SendingQueue),
    absoluteSharedSendingQueue(SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue,SmartQueue::SendingQueue),
    absoluteQueue(SmartOpt<ClipFinderOpt>::getInstance()->absoluteQueue,SmartQueue::ListeningQueue, QAmqpQueue::AutoDelete),
    absoluteSharedQueue(SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue,SmartQueue::ListeningQueue)
{
    connect(&newSegmentQueue,&SmartQueue::messageReceived,this,&Dispatcher::newSegmentMessageReceived);
    connect(&learnedClipQueue,&SmartQueue::messageReceived,this,&Dispatcher::newLearnedClipMessageReceived);
    connect(&rescanClipQueue,&SmartQueue::messageReceived,this,&Dispatcher::newRescanClipMessageReceived);
    connect(&modelDeleteQueue,&SmartQueue::messageReceived,this,&Dispatcher::deleteMessageReceived);

    connect(&absoluteSendingQueue,&SmartQueue::messageErrorSignal,this,&Dispatcher::mandatoryMessageReceived);
    connect(&absoluteQueue,&SmartQueue::messageReceived,this,&Dispatcher::absoluteQueueMessageReceived);
    connect(&absoluteSharedQueue,&SmartQueue::messageReceived,this,&Dispatcher::absoluteSharedQueueMessageReceived);

    absoluteQueue.setConsumeOption(QAmqpQueue::NoOptions);
    absoluteSharedQueue.setConsumeOption(QAmqpQueue::NoOptions);

    newSegmentQueue.setConsumeOption(QAmqpQueue::NoOptions);
    rescanClipQueue.setConsumeOption(QAmqpQueue::NoOptions);    
    learnedClipQueue.setQueueType((QAmqpQueue::QueueOption)(QAmqpQueue::AutoDelete | QAmqpQueue::Exclusive));
    modelDeleteQueue.setQueueType((QAmqpQueue::QueueOption)(QAmqpQueue::AutoDelete | QAmqpQueue::Exclusive));
    processedSegmentQueue.setConsumeOption(QAmqpQueue::NoOptions);

    int threads = SmartOpt<ClipFinderOpt>::getInstance()->recognizerCount;
    qDebug() << "Dispatcher::Dispatcher | Recognizer thread count: " << threads;

    newSegmentQueue.setPrefetch(threads);
    rescanClipQueue.setPrefetch(threads);

    absoluteQueue.setPrefetch(threads);
    absoluteSharedQueue.setPrefetch(threads);

    QThread * tempThread = new QThread();
    cutInserter.moveToThread(tempThread);
    tempThread->start();
    QMetaObject::invokeMethod(&cutInserter,"connectToDB", Qt::BlockingQueuedConnection);

    for(int i = 0; i < threads; ++i){
        QThread * tThread = new QThread();
        Recognizer * tRecognizer = new Recognizer(&segmentCache,&clipCache,&cutCache,&audioSegmentCache, &audioClipCache, &cutInserter);
        tRecognizer->moveToThread(tThread);
        tThread->start();

        connect(tRecognizer,&Recognizer::segmentProcessed,this,&Dispatcher::doneProcessing, Qt::QueuedConnection);
        connect(tRecognizer,&Recognizer::clipProcessed,this,&Dispatcher::doneProcessing, Qt::QueuedConnection);
        connect(tRecognizer,&Recognizer::newClipProcessed,this,&Dispatcher::doneProcessing, Qt::QueuedConnection);


        QMetaObject::invokeMethod(tRecognizer,"connectToDB", Qt::BlockingQueuedConnection);
        if(i==0){
            connect(tRecognizer,&Recognizer::allAdsLoaded,this,&Dispatcher::allAdsLoaded);
            QMetaObject::invokeMethod(tRecognizer,"loadAllAds", Qt::QueuedConnection);
        }

        RecognizerWorker tRecognizerWorker;
        tRecognizerWorker.recognizer = tRecognizer;
        tRecognizerWorker.recognizerThread = tThread;
        tRecognizerWorker.recognizerInUse = false;
        recognizerVec.push_back(tRecognizerWorker);
    }

    connect(&segmentCache,&SegmentCache::segmentDeleted,this,&Dispatcher::unbind);
    connect(&segmentCache,&SegmentCache::segmentLoaded,this,&Dispatcher::bind);
}

Dispatcher::~Dispatcher()
{

}

void Dispatcher::allAdsLoaded()
{
    qDebug() << "Dispatcher::allAdsLoaded";
    newSegmentQueue.start();
    learnedClipQueue.start();
    rescanClipQueue.start();
    rescanClipSendingQueue.start();
    processedSegmentQueue.start();
    modelDeleteQueue.start();

    absoluteSendingQueue.start();
    absoluteSharedSendingQueue.start();
    absoluteQueue.start();
    absoluteSharedQueue.start();

}

void Dispatcher::deleteMessageReceived(QAmqpMessage message)
{
    bool ok;
    qint64 modelId = message.payload().toLongLong(&ok);
    if(!ok) return;

    qDebug() << "Dispatcher::deleteMessageReceived | Deleting model [" << modelId << "]";
    clipCache.removeModel(modelId);
}

bool Dispatcher::unbind(qint64 segmentId)
{
    bool ok = false;
    ok = absoluteQueue.unbindRoutingKey(SmartOpt<ClipFinderOpt>::getInstance()->absoluteQueue.bindingKey+ "_" + QString::number(segmentId));

    for(auto it = boundSegments.begin(); it != boundSegments.end();){
        if(*it == segmentId) {
            it = boundSegments.erase(it);
        }else{
            ++it;
        }
    }
    return ok;
}

bool Dispatcher::bind(qint64 segmentId)
{
    bool ok = false;
    if(!boundSegments.contains(segmentId)){
        boundSegments.append(segmentId);
        ok = absoluteQueue.bindRoutingKey(SmartOpt<ClipFinderOpt>::getInstance()->absoluteQueue.bindingKey+ "_" + QString::number(segmentId));
    }

    return ok;
}

void Dispatcher::newSegmentMessageReceived(QAmqpMessage message)
{
    bool ok;
    qint64 segmentId = message.payload().toLongLong(&ok);
    newSegmentQueue.ack(message);
    absoluteSendingQueue.publish(QString::number(segmentId),SmartOpt<ClipFinderOpt>::getInstance()->absoluteQueue.bindingKey+"_"+QString::number(segmentId),true);
}

void Dispatcher::processSegment(qint64 segmentId, QAmqpMessage message)
{
    bool procesed = false;
    for(unsigned int i = 0; i < recognizerVec.size(); ++i){
        if(recognizerVec[i].recognizerInUse) continue;
        recognizerVec[i].recognizerInUse = true;
        recognizerVec[i].recognizerMessage.message = message;
        recognizerVec[i].recognizerMessage.type = SmartBuffer::newSegmentMessage;
        procesed = true;
        QMetaObject::invokeMethod(recognizerVec[i].recognizer,"process", Qt::QueuedConnection, Q_ARG(qint64,segmentId));
        break;
    }

    if(!procesed){
        messageBuffer.putMessage(message, SmartBuffer::newSegmentMessage);
    }
}

void Dispatcher::newLearnedClipMessageReceived(QAmqpMessage message)
{
    priorityMessageBuffer.putMessage(message, SmartBuffer::newLearnedMessage);
    startTaskPrioritized();
}

void Dispatcher::newRescanClipMessageReceived(QAmqpMessage message)
{
    bool ok;
    qint64 segmentId = -1;
    qint64 modelId = message.payload().toLongLong(&ok);
    if(ok){
        // Got signal from ClipModelCreator (with modelId only)
        std::vector<qint64> segments = markupDb.getSegmentsToRescanForCut(modelId);

        rescanClipQueue.ack(message);
        if(segments.size() > 0){
            qDebug() << "Dispatcher::newRescan | Segment size too large, splitting " << segments.size();

            for(int i = 0; i < segments.size(); i++){
                QString tMessage;
                segmentId = segments[i];
                tMessage = QString::number(modelId) + " " + QString::number(segmentId);
                /// NEED TO CHANGE HERE
                absoluteSendingQueue.publish(tMessage,SmartOpt<ClipFinderOpt>::getInstance()->absoluteQueue.bindingKey+"_"+QString::number(segmentId),true);
            }
            return;
        }
    }
}

void Dispatcher::absoluteQueueMessageReceived(QAmqpMessage message)
{
    processMessage(message, true);
}

void Dispatcher::absoluteSharedQueueMessageReceived(QAmqpMessage message)
{
    processMessage(message, false);
}

void Dispatcher::processMessage(QAmqpMessage message, bool priority)
{
    // Got signal from Dispatcher (with segment range)
    bool ok = true;
    QString strMessage = message.payload().toStdString().c_str();

    QStringList data = strMessage.split(" ");
    if(data.size() == 1)
    {

        ///ai.segment.new message received
        if(!ok){
            qWarning() << " newRescanClipMessageWrongFormat " << strMessage.toStdString().c_str();
            if(message.routingKey()==SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey){
                absoluteSharedQueue.reject(message,false);
            }
            else
                absoluteQueue.reject(message,false);
            return;
        }
        messageBuffer.putMessage(message, SmartBuffer::newSegmentMessage);
        startTaskPrioritized();
        return;
    }

    ///ai clip new message received

    if(!ok){
        qWarning() << " newRescanClipMessageWrongFormat " << strMessage.toStdString().c_str();
        if(message.routingKey()==SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey){
            absoluteSharedQueue.reject(message,false);
        }
        else
            absoluteQueue.reject(message,false);
        return;
    }

    if(!ok){
        qWarning() << " newRescanClipMessageWrongFormat " << strMessage.toStdString().c_str();
        if(message.routingKey()==SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey){
            absoluteSharedQueue.reject(message,false);
        }
        else
            absoluteQueue.reject(message,false);
        return;
    }

//    qDebug() << "Dispatcher::newRescanClipMessageReceived | modelId [" << modelId << segmentId;

    if(priority){
        priorityMessageBuffer.putMessage(message, SmartBuffer::newRescanMessage);
    }
    else messageBuffer.putMessage(message, SmartBuffer::newRescanMessage);

    startTaskPrioritized();
}

bool Dispatcher::recognizersAreFull()
{
    bool full = true;
    for(int i = 0; i < recognizerVec.size(); ++i){
        if(recognizerVec[i].recognizerInUse == false)full = false;
        break;
    }
    return full;
}

void Dispatcher::startTaskPrioritized()
{
    while(!recognizersAreFull() && priorityMessageBuffer.size() >0){
        startNewTaskFromBuffer(&priorityMessageBuffer);
    }

    while(!recognizersAreFull() && messageBuffer.size() >0){
        startNewTaskFromBuffer(&messageBuffer);
    }
}


void Dispatcher::startNewTaskFromBuffer(SmartBuffer * messageBuffer)
{
    if(messageBuffer->size() != 0){
        SmartBuffer::PendingMessage tMessage = messageBuffer->getMessage();
        switch(tMessage.type){
            case SmartBuffer::newRescanMessage:
            {
                bool ok;
                QString strMessage = tMessage.message.payload().toStdString().c_str();

                QStringList data = strMessage.split(" ");
                if(data.size() == 1)
                {
                    ///ai.segment.new message received
                    qint64 segmendId = data.at(0).toInt(&ok);
                    if(!ok){
                        qWarning() << " newSegmentMessageWrongFormat " << strMessage.toStdString().c_str();
                        if(tMessage.message.routingKey()==SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey){
                            absoluteSharedQueue.reject(tMessage.message,false);
                        }
                        else
                            absoluteQueue.reject(tMessage.message,false);
                        return;
                    }
                    processSegment(segmendId, tMessage.message);
                    return;
                }

                ///ai clip new message received

                qint64 modelId = data.at(0).toInt(&ok);
                if(!ok){
                    qWarning() << " newRescanClipMessageWrongFormat " << strMessage.toStdString().c_str();
                    if(tMessage.message.routingKey()==SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey){
                        absoluteSharedQueue.reject(tMessage.message,false);
                    }
                    else
                        absoluteQueue.reject(tMessage.message,false);
                    return;
                }
                qint64 segmentId = data.at(1).toInt(&ok);

                if(!ok){
                    qWarning() << " newRescanClipMessageWrongFormat " << strMessage.toStdString().c_str();
                    if(tMessage.message.routingKey()==SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey){
                        absoluteSharedQueue.reject(tMessage.message,false);
                    }
                    else
                        absoluteQueue.reject(tMessage.message,false);
                    return;
                }

                for(int i = 0; i < recognizerVec.size(); ++i){
                    if(recognizerVec[i].recognizerInUse) continue;
                    recognizerVec[i].recognizerInUse = true;
                    recognizerVec[i].recognizerMessage.message = tMessage.message;
                    recognizerVec[i].recognizerMessage.type = SmartBuffer::newRescanMessage;
                    QMetaObject::invokeMethod(recognizerVec[i].recognizer,"rescanClip", Qt::QueuedConnection,
                                                      Q_ARG(qint64,modelId), Q_ARG(qint64,segmentId));
                    break;
                }

            } break;
            case SmartBuffer::newLearnedMessage:
            {
                qint64 clipId = tMessage.message.payload().toLongLong();
                for(int i = 0; i < recognizerVec.size(); ++i){
                    if(recognizerVec[i].recognizerInUse) continue;
                    recognizerVec[i].recognizerInUse = true;
                    recognizerVec[i].recognizerMessage.message = tMessage.message;
                    recognizerVec[i].recognizerMessage.type = SmartBuffer::newRescanMessage;
                    QMetaObject::invokeMethod(recognizerVec[i].recognizer,"newClip", Qt::QueuedConnection, Q_ARG(qint64,clipId));
                    break;
                }
            } break;
            case SmartBuffer::newSegmentMessage:
            {
                qint64 segmentId = tMessage.message.payload().toLongLong();
                for(int i = 0; i < recognizerVec.size(); ++i){
                    if(recognizerVec[i].recognizerInUse) continue;
                    recognizerVec[i].recognizerInUse = true;
                    recognizerVec[i].recognizerMessage.message = tMessage.message;
                    recognizerVec[i].recognizerMessage.type = SmartBuffer::newSegmentMessage;
                    QMetaObject::invokeMethod(recognizerVec[i].recognizer,"process", Qt::QueuedConnection, Q_ARG(qint64,segmentId));
                    break;
                }
            } break;
        }
    }
}

void Dispatcher::doneProcessing()
{
    Recognizer *recognizer = qobject_cast<Recognizer*>(sender());
    for(unsigned int i = 0; i < recognizerVec.size(); ++i){
        if(recognizerVec[i].recognizerInUse && recognizerVec[i].recognizer == recognizer){
            recognizerVec[i].recognizerInUse = false;

            switch(recognizerVec[i].recognizerMessage.type){
                case SmartBuffer::newRescanMessage:
                    if(recognizerVec[i].recognizerMessage.message.routingKey()==SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey)
                        absoluteSharedQueue.ack(recognizerVec[i].recognizerMessage.message);
                    else if(QString(recognizerVec[i].recognizerMessage.message.routingKey()).contains(SmartOpt<ClipFinderOpt>::getInstance()->absoluteQueue.bindingKey))
                        absoluteQueue.ack(recognizerVec[i].recognizerMessage.message);
                    else
                        qDebug() << "strange ack behaviour new rescan message" << QString(recognizerVec[i].recognizerMessage.message.routingKey());
                break;
                case SmartBuffer::newSegmentMessage:{
                    if(recognizerVec[i].recognizerMessage.message.routingKey()==SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey)
                        absoluteSharedQueue.ack(recognizerVec[i].recognizerMessage.message);
                    else if(QString(recognizerVec[i].recognizerMessage.message.routingKey()).contains(SmartOpt<ClipFinderOpt>::getInstance()->absoluteQueue.bindingKey))
                        absoluteQueue.ack(recognizerVec[i].recognizerMessage.message);
                    else
                        qDebug() << "strange ack behaviour new segment message" << QString(recognizerVec[i].recognizerMessage.message.routingKey());
                    processedSegmentQueue.publish(recognizerVec[i].recognizerMessage.message.payload(), SmartOpt<ClipFinderOpt>::getInstance()->processedSegmentQueue.bindingKey);
                }
                break;
            }
            qDebug() << "Dispatcher::doneProcessing |" << SmartBuffer::messageTypeToStr(recognizerVec[i].recognizerMessage.type).toStdString().c_str()
                     << "buffer[" << messageBuffer.size() <<"]";
        }
    }

    startTaskPrioritized();
}

void Dispatcher::mandatoryMessageReceived(QString message, QString routingKey, QString exchangeName)
{
    qDebug() << "Failed to send message to " << message << routingKey << exchangeName << " sending to shared absolute.";
    absoluteSharedSendingQueue.publish(message,SmartOpt<ClipFinderOpt>::getInstance()->absoluteSharedQueue.bindingKey);
}

void Dispatcher::criticalError()
{
    qCritical() << "Dispatcher::criticalError quiting";
    this->exit(1);
}

}
