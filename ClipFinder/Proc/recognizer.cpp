#include "recognizer.h"
#include "smartopt.h"
#include "Opt/clipfinderopt.h"
#include "clipfinderalg.h"
#include <QDebug>
#include <QCoreApplication>
#include <QNetworkInterface>
#include <QThread>

namespace ClipFinder {

Recognizer::Recognizer(SegmentCache * segmentCache, ClipCache * clipCache, CutCache * cutCache, SegmentCache * audioSegmentCache, ClipCache * audioClipCache, CutInserter * cutInserter, QObject *parent):
    QObject(parent), clipCache(clipCache),segmentCache(segmentCache), cutCache(cutCache), audioSegmentCache(audioSegmentCache), audioClipCache(audioClipCache), cutInserter(cutInserter)
{
    db = nullptr;
    markupDb = nullptr;

    ipv4 = "";
    ipv6 = "";
    foreach (const QHostAddress &address, QNetworkInterface::allAddresses()) {
        if (address.protocol() == QAbstractSocket::IPv4Protocol && address != QHostAddress(QHostAddress::LocalHost))
             ipv4 = address.toString();
        else if (address.protocol() == QAbstractSocket::IPv6Protocol && address != QHostAddress(QHostAddress::LocalHost))
             ipv6 = address.toString();
    }
    processId =  QCoreApplication::applicationPid();
}

void Recognizer::connectToDB()
{
    db = new SmartDb(SmartOpt<ClipFinderOpt>::getInstance()->dbConfig);
    markupDb = new MarkupDB(db);
}

Recognizer::~Recognizer()
{
    delete markupDb;
    delete db;
}

void Recognizer::process(qint64 segmentId)
{
    bool ok;
    SegmentCache::ModelInstance streamInst = segmentCache->getModel(segmentId, ok, "ad.model", markupDb);
    if(!ok){
        emit segmentProcessed();
        return;
    }

    QVector<ClipCache::ClipInstance> models = clipCache->getModels();


    bool hasNoError = true;

    qint64 unitCountLog = models.size();
    qint64 startTimeLog = QDateTime::currentMSecsSinceEpoch();

    totalFramesLog = 0;
    for(auto it = models.begin(); it != models.end() && hasNoError; ++it){
        ClipFinderAlg clipFinderAlg(streamInst.model.get(), it->model.get());

        check(hasNoError, &clipFinderAlg, &streamInst, &*it,  segmentId, it->modelId);

        qDebug() << " --Done checking model " << it->modelId << " in segment " << segmentId << " [S]"
                 << " s[" << streamInst.model->size() << "|" << streamInst.model->getOffsetFrames() << "]"
                 << " c[" << it->model.get()->size() << "|" << it->model.get()->getOffsetFrames() << "]";
    }

    qint64 totalTimeLog = QDateTime::currentMSecsSinceEpoch() - startTimeLog;
    if (unitCountLog!= 0){
        if (totalTimeLog != 0)
            markupDb->writeLog("clip", unitCountLog, totalTimeLog, totalFramesLog/totalTimeLog, processId, ipv4, ipv6);
        else markupDb->writeLog("clip", unitCountLog, totalTimeLog, 0, processId, ipv4, ipv6);
    }
    emit segmentProcessed();

    qDebug() << "All done segmentProcessed";
    return;
}

qint32 Recognizer::getFrameNumber(qint64 start, qint64 current, int frame, int fps)
{
    return ((current-start)/1000)*fps + frame;
}

void Recognizer::rescanClip(qint64 modelId, qint64 minSegmentId)
{
    std::vector<qint64> segments = markupDb->getSegmentsToRescanForCut(modelId, minSegmentId, minSegmentId);
    bool hasNoError = true;

    bool ok;
    clipModel = clipCache->getModel(modelId,ok);
    if(!ok){
        clipModel = loadClip(modelId, ok, clipCache);
        if(!ok){
            ///\todo should wait form model for some time
            qWarning() << "Failed to load clip model with id " << modelId;
            emit clipProcessed();
            return;
        }
    }

    qint64 unitCountLog = segments.size();
    qint64 startTimeLog = QDateTime::currentMSecsSinceEpoch();
    totalFramesLog = 0;

    for(auto it  = segments.begin(); it != segments.end() && hasNoError; ++it){

        SegmentCache::ModelInstance streamInst = segmentCache->getModel(*it, ok, "ad.model", markupDb);
        if(!ok){
            qWarning() << "Failed to load segment "<< *it <<" to rescan clip with model id  " << modelId;
            continue;
        }


        ClipFinderAlg clipFinderAlg(streamInst.model.get(),clipModel.model.get());

//        qDebug() << " --Looking for cut with modelid " << modelId << " in segment " << *it << " [R]"
//                 << " s[" << streamInst->model.get()->size() << "|" << streamInst->model.get()->getOffsetFrames() << "]"
//                 << " c[" << clipModel.model.get()->size()  << "|" << clipModel.model.get()->getOffsetFrames()  << "]";

        check(hasNoError, &clipFinderAlg, &streamInst, &clipModel, *it, modelId);

        qDebug() << " --Done checking segment " << *it;
        segmentCache->updateSegmentUse(*it, false);
    }

    qint64 totalTimeLog = QDateTime::currentMSecsSinceEpoch() - startTimeLog;

//    qDebug() << "totalTimeLog" << totalTimeLog;
//    qDebug() << "unitCountLog" << unitCountLog;
//    qDebug() << "t/t " << totalFramesLog/totalTimeLog;

    if (unitCountLog!= 0){
        if (totalTimeLog != 0)
            markupDb->writeLog("clip", unitCountLog, totalTimeLog, totalFramesLog/totalTimeLog, processId, ipv4, ipv6);
        else markupDb->writeLog("clip", unitCountLog, totalTimeLog, 0, processId, ipv4, ipv6);
    }

    emit clipProcessed();
    //qDebug() << "All done clipProcessed";
    return;
}

void Recognizer::check(bool & hasNoError, ClipFinderAlg * clipFinderAlg, SegmentCache::ModelInstance * streamInst, ClipCache::ClipInstance * clipModel, qint64 segmentId, qint64 modelId)
{
    std::vector<MarkupDB::TimeInfo> intervals = getRescanCutTimes(segmentId);
    int idebug =0;
    qint64 start = 0;
    qint64 end = 0;
    qint64 start2 = 0;

    qint64 streamStart = streamInst->model->getStartTime();
    qint64 streamEndFrame = streamInst->model->size();

    int fps = clipModel->model.get()->getFPS();

    bool last = false;
    bool finish = false;
    auto interv = intervals.begin();

    //qDebug() << "intervals.size()" <<intervals.size();

    if(intervals.size()!=0)
    while(!finish){
        idebug++;
        start = start2;
        if (interv == intervals.end()) last = true;

        if(!last){
            end = getFrameNumber(streamStart, interv->startsAt, interv->startFrame, fps);
            start2 = getFrameNumber(streamStart,interv->endsAt, interv->endFrame, fps);
        }
        else{
            end = streamEndFrame;
            finish = true;
        }

        start++;
        end--;
        if (end-start+50 < clipModel->model.get()->size() || end < 0 || start < 0){
            interv++;
            continue;
        }
        int s1 = start - 25;
        if (s1<0)s1=0;
        int e1 = end + 25;
        if (e1 > streamEndFrame) e1 = streamEndFrame;
        clipFinderAlg->setInterval(s1, e1);

        totalFramesLog += end - start;

        findClips(hasNoError, clipFinderAlg, clipModel, segmentId, modelId, streamInst);
        interv++;
    }
    else{
        clipFinderAlg->setInterval(0, streamEndFrame);
        totalFramesLog = streamEndFrame;

        findClips(hasNoError, clipFinderAlg, clipModel, segmentId, modelId, streamInst);
    }
}

void Recognizer::findClips(bool & hasNoError, ClipFinderAlg * clipFinderAlg, ClipCache::ClipInstance * clipModel, qint64 segmentId, qint64 modelId, SegmentCache::ModelInstance * streamInst)
{
    ClipFinderAlg::ClipPosition clipPos;
    while(hasNoError && clipFinderAlg->findNext(clipPos))
    {
        MarkupDB::CutInfo sCut;

        //Checks in which segment clipPos starts
        if(streamInst->prevSegmentId!=-1 && streamInst->prevSegmentFragment!=-1 && clipPos.startsAt < streamInst->firstFragment)
        {
            qDebug() << "Found clip on segment border" <<segmentId << streamInst->prevSegmentId;        
            sCut.videoSegmentId = streamInst->prevSegmentId;
            sCut.prevSegmentFragStart=streamInst->prevSegmentFragment;
            sCut.prevSegment=sCut.videoSegmentId;
            sCut.hintAddedBy  = "ClipFinder segment border";
        }
        else
        {
            qDebug()<<segmentId<<"next"<<clipPos.startsAt;
            sCut.videoSegmentId = segmentId;
        }
        sCut.realSegId=segmentId;
        sCut.cutId = -1;
        sCut.startFrame = clipPos.startFrame;
        sCut.startsAt = clipPos.startsAt;
        sCut.endFrame = clipPos.endFrame;
        sCut.endsAt = clipPos.endsAt;
        sCut.recognitionType = MarkupDB::AUTO;
        sCut.specificationId = clipModel->specificationId;
        sCut.recognitionConfidence = 1;
        sCut.hintType = MarkupDB::NONE;
        sCut.recognitionModelId = modelId;
        sCut.cutType = MarkupDB::VIDEO_CUT;
        sCut.audioConfidence = -1;
        sCut.audioType = MarkupDB::MATCHING_AUDIO;
        sCut.thresholdValue = clipPos.diffValue;
        sCut.hasConflict = markupDb->modelConflictExists(modelId);

        bool ok = false;

        SegmentCache::ModelInstance audioStream =  audioSegmentCache->getModel(segmentId, ok, "ad.audio", markupDb);
        if(ok){
            ClipCache::ClipInstance audioClip = loadClip(modelId, ok, audioClipCache, -1, "cut_%1.audio");
            if(ok) sCut.audioConfidence = clipFinderAlg->checkPeakSound(clipPos, audioStream.model.get(), audioClip.model.get());
            if(sCut.audioConfidence < SmartOpt<ClipFinderOpt>::getInstance()->threshAudioConf) sCut.audioType = MarkupDB::WRONG_AUDIO;
        }

        int dbResult = cutInserter->saveVideoCut(sCut);
        qDebug() << "RESCAN model " << modelId
                 << "  found at " << QDateTime::fromMSecsSinceEpoch(clipPos.startsAt).toString() << clipPos.startFrame
                 << "  dbResult "<< dbResult << " specificationId: " << sCut.specificationId << "     diffValue: " << clipPos.diffValue <<"  audio: " << sCut.audioConfidence << "\n";
        if(dbResult == -2){
            qWarning() << "Failed searching for cut .. specification didnt exist when saving. Deleteing cut from cache and aborting.";
            hasNoError = false;
        }
        else cutCache->updateCache = true;
        audioSegmentCache->updateSegmentUse(segmentId, false);
    }
}

std::vector<MarkupDB::TimeInfo> Recognizer::getRescanCutTimes(qint64 segmentId)
{
    bool ok = false;
    std::vector<MarkupDB::TimeInfo> rez;
    if(!cutCache->updateCache)  rez = cutCache->getSegmentTime(segmentId, ok);

    if (ok)
    {
        qDebug() << "returning rez. size: " << rez.size();
        return rez;
    }
    else
    {
        auto cutInfos = markupDb->getRescanCutTimes(segmentId);
        cutCache->addCutInfo(segmentId, cutInfos);
        return cutInfos;
    }
}

void Recognizer::newClip(qint64 modelId)
{
    bool ok;
    ClipCache::ClipInstance clipModel = clipCache->getModel(modelId,ok);
    if(!ok){
        clipModel = loadClip(modelId, ok, clipCache);
        if(!ok){
            ///\todo should wait form model for some time
            qWarning() << "Failed to load clip model " << modelId;
            emit newClipProcessed();
            return;
        }
    }
    emit newClipProcessed();
    return;
}

void Recognizer::loadAllAds()
{
    std::vector<MarkupDB::CutInfo> cuts = markupDb->getAllCutsWithModels();

    qint64 successes = 0;
    for(auto it = cuts.begin(); it != cuts.end(); ++it){
        bool ok;
        loadClip(it->recognitionModelId, ok, clipCache, it->specificationId);
        if(ok) successes++;
        loadClip(it->recognitionModelId, ok, audioClipCache, it->specificationId, "cut_%1.audio");
    }
    qDebug() << "Recognizer::loadAllAds loaded " << successes << " out of " << cuts.size() << " that were available!";
    emit allAdsLoaded();
}

ClipCache::ClipInstance Recognizer::loadClip(qint64 modelId, bool &ok, ClipCache *clipCache, qint64 specificationId, QString modelname)
{
    qint64 specification = specificationId;
    MarkupDB::CutInfo info;
    if(specificationId == -1){
        info  = markupDb->getCutInfoByModel(modelId);
        specification = info.specificationId;
    }

    ok = true;

    ClipCache::ClipInstance instance;
    ///\todo refactoring needed clipId == it->cutId jet names different
    instance.clipId = info.cutId;
    instance.modelId = modelId;
    instance.specificationId = specification;
    instance.model.reset(new TVModel());
    ok = instance.model.get()->readIntoSelf(
                QString("%1%2").arg(SmartOpt<ClipFinderOpt>::getInstance()->adModelConfig.clipSavePath)
                .arg("/"+modelname.arg(modelId)));
    if(ok && clipCache){
        clipCache->pushModel(instance);
    }
    return instance;
}

}
