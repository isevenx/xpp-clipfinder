#ifndef CLIPFINDERALG_H
#define CLIPFINDERALG_H

#include "tvmodel.h"

class ClipFinderAlg
{
public:
    ClipFinderAlg(TVModel *stream, TVModel *clip);
//    ~ClipFinderAlg();
    typedef struct {
        qint64 startsAt;
        qint64 endsAt;
        qint32 startFrame;
        qint32 endFrame;

        qint64 bestMatchFrame;
        qint64 diffValue;
    } ClipPosition;

     bool findNext(ClipPosition &clipPos);
     void setInterval(qint32 start, qint32 end);
     qint64 checkPos(qint64 startFrame);
     bool compareModels();
     bool compareAudioPeaks();

     ///
     /// \brief checkPeakSound compares clips using peak difference
     /// \param clipPos with bestFrame matching found video clip starting position
     /// \param audioStream pointer to loaded audio stream
     /// \param audioClip pointer to loaded clip
     /// \return matching confidence
     ///
     qint64 checkPeakSound(ClipPosition &clipPos, TVModel *audioStream, TVModel *audioClip);

private:
     TVModel * stream;
     TVModel * clip;
     qint32  currentFrame;

     int minFramesComp;
     int maxDiff;
     int treshDiff;
     int maxPeakFramesRegion;

     int maxCompFrames;
     int startFrameCount;
     int endFrameCount;

     //qint32 intervStart;
     qint32 intervEnd;

     int audioThresh, threshAudioConf, audioShiftRegion;
     float peakCoef;

     double calcAvgIntensity(TVModel *stream, qint32 frameNr, qint32 clipSize);
};

#endif // CLIPFINDERALG_H
