#ifndef CUTINSERTER_H
#define CUTINSERTER_H

#include <QObject>
#include <QMutex>

#include "smartdb.h"
#include "markupdb.h"
#include "smartopt.h"
#include "admodelloader.h"

class CutInserter : public QObject
{
    Q_OBJECT
public:
    explicit CutInserter(QObject *parent = 0);

public slots:

    ///
    /// \brief saveVideoCut saves videoCut (expected auto), checks for near cuts and corrects / tries to find precise border
    /// \param info
    /// \return insertNewCut db result
    ///
    int saveVideoCut(MarkupDB::CutInfo &info);
    void connectToDB();

private:

    SmartDb * db;
    MarkupDB * markupDb;
    qint64 shift;
    qint64 shiftSize;
    QString modelFilePath, modelTimeFormat;

    const qint64 pixThreshold = 20;

    qint64 checkBorder(TVModel * newModel, TVModel * oldModel, qint64 newTime, qint64 oldTime, bool start);
    double calcFrameDiff(char * firstFrame, char * secondFrame, int frameSize);

    mutable QMutex mutex;
};

#endif // CUTINSERTER_H
