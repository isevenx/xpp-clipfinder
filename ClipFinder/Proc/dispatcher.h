#ifndef DISPATCHER_H
#define DISPATCHER_H

#include <QObject>
#include <QCoreApplication>
#include <QThread>
#include <QTimer>

#include "smartqueue.h"

#include "Struct/smartbuffer.h"
#include "Proc/recognizer.h"

namespace ClipFinder {

///
/// \brief The Dispatcher class Main entrypoint of executable for ClipFinder
/// All settings and arguments should be processed before
///
class Dispatcher : public QCoreApplication
{
    Q_OBJECT
public:
    Dispatcher(int &argc, char **argv);
    ~Dispatcher();
private slots:
    void newSegmentMessageReceived(QAmqpMessage message);
    void newLearnedClipMessageReceived(QAmqpMessage message);
    void newRescanClipMessageReceived(QAmqpMessage message);
    void deleteMessageReceived(QAmqpMessage message);
    void criticalError();
    void allAdsLoaded();
    void doneProcessing();

    bool unbind(qint64 segmentId);
    void mandatoryMessageReceived(QString message, QString routingKey, QString exchangeName);
    void absoluteQueueMessageReceived(QAmqpMessage message);
    void absoluteSharedQueueMessageReceived(QAmqpMessage message);
    void processMessage(QAmqpMessage message, bool priority  = false);

private:
    SmartDb db;
    MarkupDB markupDb;

    SmartQueue newSegmentQueue;
    SmartQueue learnedClipQueue;
    SmartQueue rescanClipQueue;
    SmartQueue rescanClipSendingQueue;
    SmartQueue processedSegmentQueue; // For jingleHeatmap
    SmartQueue modelDeleteQueue;

    SmartQueue absoluteSharedQueue;
    SmartQueue absoluteQueue;
    SmartQueue absoluteSendingQueue;
    SmartQueue absoluteSharedSendingQueue;

    SmartBuffer messageBuffer;
    SmartBuffer priorityMessageBuffer;


    struct RecognizerWorker{
        QThread * recognizerThread;
        Recognizer * recognizer;
        bool recognizerInUse;
        SmartBuffer::PendingMessage recognizerMessage;
    };

    std::vector<RecognizerWorker> recognizerVec;

    CutInserter cutInserter;

    ClipCache clipCache, audioClipCache;
    CutCache cutCache;
    SegmentCache segmentCache, audioSegmentCache;

    bool bind(qint64 segmentId);
    void processSegment(qint64 segmentId, QAmqpMessage message);
    void startNewTaskFromBuffer(SmartBuffer *messageBuffer);
    void startTaskPrioritized();
    bool recognizersAreFull();
    QVector<qint64> boundSegments;
};

}

#endif // DISPATCHER_H
