HEADERS += \
    $$PWD/dispatcher.h \
    $$PWD/recognizer.h \
    $$PWD/cutinserter.h \
    $$PWD/clipfinderalg.h

SOURCES += \
    $$PWD/dispatcher.cpp \
    $$PWD/recognizer.cpp \
    $$PWD/cutinserter.cpp \
    $$PWD/clipfinderalg.cpp
