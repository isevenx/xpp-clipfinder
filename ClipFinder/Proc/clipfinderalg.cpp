#include "clipfinderalg.h"
#include "smartopt.h"
#include "Opt/clipfinderopt.h"

#include <vector>
#include <QDebug>
#include <QDateTime>
#include <memory>

ClipFinderAlg::ClipFinderAlg(TVModel* stream, TVModel * clip):
    currentFrame(0), stream(stream), clip(clip)
{
    if(stream->getOffsetFrames() != 0){
        qWarning() << "Wrong stream model - model has offset";
    }

    minFramesComp = std::min(10,clip->size());
    maxDiff = SmartOpt<ClipFinderOpt>::getInstance()->maxDiff; /// not usded
    treshDiff = SmartOpt<ClipFinderOpt>::getInstance()->treshDiff;
    maxPeakFramesRegion = SmartOpt<ClipFinderOpt>::getInstance()->maxPeakFramesRegion;

    maxCompFrames = SmartOpt<ClipFinderOpt>::getInstance()->maxCompFrames;
    startFrameCount = SmartOpt<ClipFinderOpt>::getInstance()->startFrameCount;
    endFrameCount = SmartOpt<ClipFinderOpt>::getInstance()->endFrameCount;
    threshAudioConf = SmartOpt<ClipFinderOpt>::getInstance()->threshAudioConf;
    audioShiftRegion = SmartOpt<ClipFinderOpt>::getInstance()->audioShiftRegion;

    audioThresh = 90;
    peakCoef = 0.9;
}

void ClipFinderAlg::setInterval(qint32 start, qint32 end)
{
    currentFrame = start;
    intervEnd = end;
}

bool ClipFinderAlg::findNext(ClipPosition &clipPos)
{
    bool found = false;
    qint64 currBestMatch = -1;
    qint64 currBestMatchPos = currentFrame;
    qint64 maxFrame = intervEnd-clip->size();

    while(!found && currentFrame < maxFrame)
    {
        qint64 tMatch = checkPos(currentFrame);
        if((tMatch != -1) && (currBestMatch == -1 || tMatch < currBestMatch))
        {
            currBestMatch = tMatch;
            currBestMatchPos = currentFrame;
        }else if(currBestMatch >= 0 && (tMatch < 0 || currentFrame-currBestMatchPos > maxPeakFramesRegion || currentFrame > intervEnd))
        {
            currentFrame = currBestMatchPos;
            // Match found at currBestMatchPos
            ///\todo assumption that stream start is whole sec
            clipPos.startsAt = stream->getStartTime() + qint64(currentFrame/stream->getFPS())*1000;
            clipPos.startFrame = currentFrame%stream->getFPS();
            // CLIP CORRECTION !!
            clipPos.endsAt = stream->getStartTime() + qint64((currentFrame+clip->size()+1)/stream->getFPS())*1000;
            clipPos.endFrame = (currentFrame+clip->size()+1)%stream->getFPS();
            clipPos.bestMatchFrame = currBestMatchPos;
            clipPos.diffValue = currBestMatch;
            found = true;
        }
        currentFrame++;
        //qDebug() << "currentFrame "<< currentFrame;
    }

    currentFrame += clip->size();

    return found;
}

bool ClipFinderAlg::compareModels()
{
    bool found = false;

    qint64 currBestMatch = -1;
    qint64 currBestMatchPos = currentFrame;
    qint64 maxFrame = intervEnd;//-clip->size();

    while(!found && currentFrame < maxFrame)
    {
        qint64 tMatch = checkPos(currentFrame);
        if((tMatch != -1) && (currBestMatch == -1 || tMatch < currBestMatch))
        {
            currBestMatch = tMatch;
            currBestMatchPos = currentFrame;
        }else if(currBestMatch >= 0 && (tMatch < 0 || currentFrame-currBestMatchPos > maxPeakFramesRegion || currentFrame > intervEnd))
        {
            currentFrame = currBestMatchPos;
            ///\todo assumption that stream start is whole sec
            found = true;
            currBestMatch = -1;
        }
        currentFrame++;
    }
    currentFrame += clip->size();

    return found;
}

qint64 ClipFinderAlg::checkPos(qint64 startFrame)
{
    if(startFrame + clip->size() > stream->size()) return -1;

    const char * refFrame = stream->getFrame(startFrame);
    if (refFrame == 0){
        //qDebug() << " NULL FRAME stream: " << startFrame+offset << " " << (bool)(refFrame == 0 ) << "  clip: " << offset << " " << (bool)(clipFrame == 0 );
        //offset++;
        return -1;
    }
    const char * clipFrame = clip->getFrame(0);
    if(clipFrame == 0){
        //qDebug() << " NULL FRAME stream: " << startFrame+offset << " " << (bool)(refFrame == 0 ) << "  clip: " << offset << " " << (bool)(clipFrame == 0 );
        //offset++;
        return -1;
    }
    qint64 curDiffSum = 0;
    qint64 curDiffCount = 0;

    qint32 frameSize = stream->getFrameSize();

    qint64 offset = 0;

    int clipSz = clip->size();
    int skip = clipSz/maxCompFrames;
    if (skip==0)skip++;

    while((curDiffCount < minFramesComp || curDiffSum/curDiffCount <= treshDiff) && (offset) <= clipSz)
    {
        qint32 tDiff = 0;
        if (offset > startFrameCount && clipSz-offset > endFrameCount)
        {
            offset += skip;
        }

        const char * refFrame = stream->getFrame(startFrame+offset);
        if (refFrame == 0){
            //qDebug() << " NULL FRAME stream: " << startFrame+offset << " " << (bool)(refFrame == 0 ) << "  clip: " << offset << " " << (bool)(clipFrame == 0 );
            offset++;
            continue;
        }
        const char * clipFrame = clip->getFrame(offset);
        if(clipFrame == 0){
            //qDebug() << " NULL FRAME stream: " << startFrame+offset << " " << (bool)(refFrame == 0 ) << "  clip: " << offset << " " << (bool)(clipFrame == 0 );
            offset++;
            continue;
        }

        for(int px = 0; px < frameSize; px++){
            tDiff += std::abs((uchar)refFrame[px]-(uchar)clipFrame[px]);
        }

        curDiffSum += tDiff;
        curDiffCount++;
        offset++;
    }

    if(curDiffCount > minFramesComp && curDiffSum/curDiffCount < treshDiff) return curDiffSum/curDiffCount;
    return -1;
}

bool ClipFinderAlg::compareAudioPeaks()
{
    bool found = false;
    ClipPosition clipPos;
    clipPos.bestMatchFrame = 0;

    do{
        int conf = checkPeakSound(clipPos, clip, stream);
        if(conf > threshAudioConf){
            found = true;
            break;
        }
        clipPos.bestMatchFrame += audioShiftRegion;
    }
    while(clipPos.bestMatchFrame + audioShiftRegion < stream->size());

    return found;
}

double ClipFinderAlg::calcAvgIntensity(TVModel *stream, qint32 frameNr, qint32 clipSize)
{
    double avgVariance = 0;
    int emptyFrames = 0;
    int frameSize = stream->getFrameSize();
    for(int i = 0; i < clipSize; i++) {
        const char *frame = stream->getFrame(frameNr+i);
        if(!frame) { emptyFrames++; continue; }
        for(int px = 0; px < frameSize; px++)
            avgVariance += uchar(frame[px]);
    }
    avgVariance /= double(clipSize-emptyFrames);
    avgVariance /= double(frameSize);
    return avgVariance*peakCoef;
}

qint64 ClipFinderAlg::checkPeakSound(ClipPosition &clipPos, TVModel *audioStream, TVModel *audioClip)
{
    if(clipPos.bestMatchFrame + audioClip->size() > audioStream->size()) return -1;

    const char * refFrame = audioStream->getFrame(clipPos.bestMatchFrame);
    const char * clipFrame = audioClip->getFrame(0);
    if(!refFrame || !clipFrame) return -1;

    qint32 frameSize = audioStream->getFrameSize();
    qint64 offset = -1;
    int clipSz = audioClip->size();
    int startFrameNr = clipPos.bestMatchFrame, direction = -1, shift = -1;


    double clipVariance = 0, streamVariance = 0;
    qint64 confidence = -1;
    qint64 peakDiffCount = 0;
    qint64 clipPeakCount = 0;

    QTime bench;
    bench.start();

    // Calc variance for clip and stream
    clipVariance = calcAvgIntensity(audioClip, 0, clipSz);
    streamVariance = calcAvgIntensity(audioStream, clipPos.bestMatchFrame, clipSz);

    // Shift comparing positions forward, then backward
    while(shift <= audioShiftRegion) {

        // Change direction
        direction *= -1;
        if(direction == 1) shift++;
        startFrameNr = clipPos.bestMatchFrame + (shift*direction);

        // Recalculating stream variance with new frame
        streamVariance = calcAvgIntensity(audioStream, startFrameNr, clipSz);


        // Calculating peak difference        
        offset = -1;
        peakDiffCount = 0;
        clipPeakCount = 1;
        while(++offset < clipSz)
        {
            double refPeak = 0, clipPeak = 0;

            const char * refFrame = audioStream->getFrame(startFrameNr+offset);
            const char * clipFrame = audioClip->getFrame(offset);
            if(!refFrame || !clipFrame) continue;

            for(int px = 0; px < frameSize; px++) {
                refPeak += (uchar)refFrame[px];
                clipPeak += (uchar)clipFrame[px];
            }
            refPeak /= frameSize;
            clipPeak /= frameSize;

            if((clipPeak > clipVariance)) clipPeakCount++;
            if((clipPeak > clipVariance) != (refPeak > streamVariance)) peakDiffCount++;
        }

        // Save result        
        int temp = (100 - ((peakDiffCount/double(clipPeakCount))*100));
        if(clipPeakCount == 1) temp = 0;
        if(temp > confidence) confidence = temp;
        if(confidence > audioThresh) break;
    }

    return (confidence>0)?confidence:0;
}
