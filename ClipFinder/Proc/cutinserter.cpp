#include "cutinserter.h"

#include <float.h>
#include <climits>
#include <QThread>
#include <QDebug>
#include <QMutex>

#include "smartopt.h"
#include "Opt/clipfinderopt.h"

CutInserter::CutInserter(QObject *parent) : QObject(parent)
{
    db = nullptr;
    markupDb = nullptr;
    shift = SmartOpt<ClipFinderOpt>::getInstance()->cutInserterShift;
    shiftSize = SmartOpt<ClipFinderOpt>::getInstance()->cutInserterShiftSize;
    modelFilePath = SmartOpt<ClipFinderOpt>::getInstance()->adModelConfig.savePath+"/%1/ad.model";
    modelTimeFormat = SmartOpt<ClipFinderOpt>::getInstance()->adModelConfig.modelTimeFormat;
}

void CutInserter::connectToDB()
{
    db = new SmartDb(SmartOpt<ClipFinderOpt>::getInstance()->dbConfig);
    markupDb = new MarkupDB(db);
}


int CutInserter::saveVideoCut(MarkupDB::CutInfo &info)
{
    QMutexLocker locker(&mutex);

    bool correctStart = true, correctEnd = true;
    qint64 startTime = info.startsAt+info.startFrame*40;
    qint64 endTime = info.endsAt+info.endFrame*40;
    int dbResult = -1;
    MarkupDB::CutInfo refInfo;
    TVModel newModel, refModel;


    if(!SmartOpt<ClipFinderOpt>::getInstance()->cutInserterEnableCheck){
        qWarning() << "CutInserter::saveVideoCut | Not checking! Saving cut as is!";
        goto SavingCut;
    }


    // Correcting cuts from database
    if(markupDb->correctCutStart(info)) correctStart = false;
    if(markupDb->correctCutEnd(info)) correctEnd = false;
    if((!correctStart && !correctEnd) || info.recognitionModelId < 1) goto SavingCut;


    // Loading new clip
    if(!AdModelLoader::loadIntoModel(&newModel,
                                    QDateTime::fromMSecsSinceEpoch(startTime).addMSecs(-(shift+shiftSize)*40),
                                    QDateTime::fromMSecsSinceEpoch(endTime).addMSecs((shift+shiftSize)*40),
                                    modelFilePath,
                                    modelTimeFormat.arg(markupDb->getChannelId(info.videoSegmentId))))
        goto SavingCut;

    // Loading manual cut
    refInfo = markupDb->getCutInfoByModel(info.recognitionModelId);
    if(refInfo.cutId < 1 || !AdModelLoader::loadIntoModel(&refModel,
                                                         QDateTime::fromMSecsSinceEpoch(refInfo.startsAt+refInfo.startFrame*40).addMSecs(-(shift+shiftSize)*40),
                                                         QDateTime::fromMSecsSinceEpoch(refInfo.endsAt+refInfo.endFrame*40).addMSecs((shift+shiftSize)*40),
                                                         modelFilePath,
                                                         modelTimeFormat.arg(markupDb->getChannelId(refInfo.videoSegmentId))))
        goto SavingCut;


    // Correcting borders with models
    if(correctStart){
        startTime = checkBorder(&newModel, &refModel, startTime, refInfo.startsAt+refInfo.startFrame*40, true);
        info.startFrame = startTime%1000;
        info.startsAt = startTime-info.startFrame;
        info.startFrame /= 40;
    }
    if(correctEnd){
        endTime = checkBorder(&newModel, &refModel, endTime, refInfo.endsAt+refInfo.endFrame*40, false);
        info.endFrame = endTime%1000;
        info.endsAt = endTime-info.endFrame;
        info.endFrame /= 40;
    }

SavingCut:
    {
        if(SmartOpt<ClipFinderOpt>::getInstance()->cutInserterEnableCheck) markupDb->correctPatternCutsAroundVideoCut(info);
        dbResult = markupDb->insertNewCut(info);
    }

    newModel.clear();
    refModel.clear();

    return dbResult;
}

qint64 CutInserter::checkBorder(TVModel *newModel, TVModel *oldModel, qint64 newTime, qint64 oldTime, bool start)
{
    if(!newModel || !newModel->size() || newModel->getStartTime() < 1) return newTime;
    if(!oldModel || !oldModel->size() || oldModel->getStartTime() < 1) return newTime;

    // ALG CORRECTION !!
    qint64 newStartFrame = (newTime-newModel->getStartTime())/40 - start;
    qint64 oldStartFrame = (oldTime-oldModel->getStartTime())/40 - start;
    qint64 frameSize = newModel->getHeight() * newModel->getWidth();

    int it = 0, bestGap = INT_MAX, bestIt = 0;
    double bestDiff = DBL_MAX;
    int direction = ((start)?-1:1);
    int startIt = shift*(-direction), endIt = shift*(direction);



    // Moving Block(shiftSize) from innerBorder to outterBorder
    for(it = startIt; it != endIt; it+=direction) {

        int k = shiftSize*(-direction);
        bool foundGap = false;
        double diff = 0, avgDiff = 0, prevDiff = 0;
        char * prevFrame = 0;


        // Checking block at current position
        while(k!=0) {
            k += direction;

            // Getting frames
            char * newFrame = (char*)newModel->getFrame(newStartFrame+it+k);
            char * oldFrame = (char*)oldModel->getFrame(oldStartFrame+k);
            if(!newFrame || !oldFrame) continue;


            // Counting difference
            diff = calcFrameDiff(newFrame, oldFrame, frameSize);
            prevDiff = calcFrameDiff(prevFrame, newFrame, frameSize);


            // If difference bigger that threshold - frame change found
            if(diff > pixThreshold){
                if(prevDiff > pixThreshold) foundGap = true;
                break;
            }
            else avgDiff += diff;

            prevFrame = newFrame;
        }

        k = abs(k);
        if((shiftSize-k) == 0) avgDiff = DBL_MAX;
        else avgDiff /= (double)(shiftSize-k);

        // Saving block with minimal gap and minimal avg difference before gap
        if((foundGap) && (avgDiff != DBL_MAX) && (k == 0) && (avgDiff < bestDiff)){
                bestGap = k;
                bestIt = it;
                bestDiff = avgDiff;
        }
    }

    if(bestIt) qDebug() << "CutInserter::checkBorder | Border [" << ((start)?"start":"end") << "] shifted by [" << bestIt << "frames ]           bestDiff [" << bestDiff << "]";
    return newTime+(bestIt*40);
}

double CutInserter::calcFrameDiff(char *firstFrame, char *secondFrame, int frameSize)
{
    if(!firstFrame || !secondFrame) return 0;
    double diff = 0;

    for(int pix = 0; pix < frameSize; pix++) {
        diff += abs((uchar)firstFrame[pix] - (uchar)secondFrame[pix]);
    }
    diff/=double(frameSize);
    return diff;
}

