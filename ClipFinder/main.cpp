#include "Proc/dispatcher.h"

#include <QCommandLineParser>
#include <QObject>
#include <QDateTime>
#include <QFile>
#include <QDebug>
#include <QMessageLogContext>
#include "smartopt.h"
#include "Opt/clipfinderopt.h"

#ifdef CRASHDUMPS
#include "backward.h"
backward::SignalHandling sh;
#endif

void customMessageHandler(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toLocal8Bit();
    int logLevel = SmartOpt<ClipFinderOpt>::getInstance()->logLevel;
    QString txt;
    switch (type) {
    case QtDebugMsg:
        txt = QString("%1 | Debug    | %2")
                .arg(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm ss.zzz"))
                .arg(localMsg.constData());
        fprintf(stderr,"%s",QString("%1\n").arg(txt).toStdString().c_str());
        if(logLevel <= 3) return;
        break;
    case QtWarningMsg:
        txt = QString("%1 | Warning  | %2")
                .arg(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm ss.zzz"))
                .arg(localMsg.constData());
        fprintf(stderr,"%s",QString("%1\n").arg(txt).toStdString().c_str());
        if(logLevel <= 2) return;
        break;
    case QtCriticalMsg:
        txt = QString("%1 | Critical | %2 (%3:%4, %5)")
                .arg(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm ss.zzz"))
                .arg(localMsg.constData())
                .arg(context.file)
                .arg(context.line)
                .arg(context.function);
        fprintf(stderr,"%s",QString("%1\n").arg(txt).toStdString().c_str());
        if(logLevel <= 1) return;
        break;
    case QtFatalMsg:
        txt = QString("%1 | Fatal    | %3 (%3:%4, %5)")
                .arg(QDateTime::currentDateTime().toString("yyyy.MM.dd hh:mm ss.zzz"))
                .arg(localMsg.constData())
                .arg(context.file)
                .arg(context.line)
                .arg(context.function);
        fprintf(stderr,"%s",QString("%1\n").arg(txt).toStdString().c_str());
        if(logLevel <= 0) abort();
    }

    if(txt.isEmpty()) return;

    QFile outFile(SmartOpt<ClipFinderOpt>::getInstance()->logFile);
    if(outFile.open(QIODevice::WriteOnly | QIODevice::Append)){
        QTextStream ts(&outFile);
        ts << txt << endl;
    }else{
        qWarning() << "Failed to open log file for writing  - " << SmartOpt<ClipFinderOpt>::getInstance()->logFile;
    }

    if(type == QtFatalMsg) abort();
}

bool parseClArguments(int argc, char *argv[])
{
    QStringList arguments;
    for (int a = 0; a < argc; ++a) {
        arguments << QString::fromLocal8Bit(argv[a]);
    }

    // ========================================== DESCRIBE ===========================================

    QCommandLineParser parser;
    parser.setApplicationDescription("ClipFinder - identifies repeated clips in stream");

    QCommandLineOption settingsFileOption(QStringList() << "settingsFile" << "settings file path",
               QObject::tr("main", "Provides settings file."),
               QObject::tr("main", "path - string"));
    parser.addOption(settingsFileOption);

    // ========================================== PARSE   ===========================================

    parser.process(arguments);

    // ========================================== PROCESS ===========================================

    // ====================================================== settingsFile
    if(parser.isSet(settingsFileOption)){
        qDebug() << "Custom settings file supplied: " << parser.value(settingsFileOption);
        SmartOpt<ClipFinderOpt>::getInstance(parser.value(settingsFileOption));
    }else{
        SmartOpt<ClipFinderOpt>::getInstance();
    }

    qDebug() << "ClipFinder Started";

    return true;
}

int main(int argc, char *argv[])
{
    QCoreApplication::setOrganizationName("Applyit");
    QCoreApplication::setOrganizationDomain("applyit.lv");
    QCoreApplication::setApplicationName("ClipFinder");

    if(!parseClArguments(argc, argv)) return 1;

    qInstallMessageHandler(customMessageHandler);

    ClipFinder::Dispatcher a(argc, argv);

    return a.exec();
}
