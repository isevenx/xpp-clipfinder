#ifndef CLIPFINDEROPT_H
#define CLIPFINDEROPT_H

#include "opt.h"
#include "queueconnectionopt.h"
#include "markupmodelopt.h"
#include "smartdb.h"

class ClipFinderOpt : public Opt
{
public:
    ClipFinderOpt();

    // vv----------- Per instance settings --------------vv
    // ^^------------- not saved to file ----------------^^

    QString logFile;
    qint64 logLevel;
    qint64 maxClipRescanSegments;
    QueueConnectionOpt newSegmentQueue;
    QueueConnectionOpt newSegmentQueueCutTimes;
    QueueConnectionOpt learnedClipQueue;
    QueueConnectionOpt rescanClipQueue;
    QueueConnectionOpt absoluteQueue;
    QueueConnectionOpt absoluteSharedQueue;
    QueueConnectionOpt deadLetterQueue;
    QueueConnectionOpt processedSegmentQueue;
    QueueConnectionOpt modelDeleteQueue;

    MarkupModelOpt adModelConfig;
    SmartDbOpt dbConfig;

    qint64 maxDiff;
    qint64 treshDiff;
    qint64 maxPeakFramesRegion;

    qint64 threshAudioConf;
    qint64 audioShiftRegion;

    qint64 cutInserterShift;
    qint64 cutInserterShiftSize;
    bool cutInserterEnableCheck;

    qint64 maxCompFrames;
    qint64 startFrameCount;
    qint64 endFrameCount;

    int recognizerCount;

    bool setJSON(QJsonValue value);
    QJsonValue getJSON();
    void setDefaults();
    void setEnvironment();

    bool operator==(ClipFinderOpt &other);
    inline bool operator!=(ClipFinderOpt &other){return !(*this == other);}
};


#endif // CLIPFINDEROPT_H
