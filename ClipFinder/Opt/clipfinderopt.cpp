#include "clipfinderopt.h"

#include <QThread>

ClipFinderOpt::ClipFinderOpt()
{
    filename = "clipfinder.json";
    setDefaults();
}

void ClipFinderOpt::setDefaults()
{
    maxDiff = 500;
    treshDiff = 200;
    maxPeakFramesRegion = 100;
    maxCompFrames = 100;
    startFrameCount = 5;
    endFrameCount = 5;

    cutInserterShift = 12;
    cutInserterShiftSize = 7;
    cutInserterEnableCheck = true;

    threshAudioConf = 56;
    audioShiftRegion = 100;

    recognizerCount = std::max(QThread::idealThreadCount(),1);

    logLevel = 4;
    logFile = "/var/log/smart/clipfinder.log";
    maxClipRescanSegments = 1;

    newSegmentQueue.type = QueueConnectionOpt::Direct;
    newSegmentQueue.exchange = "ai.segment.new";
    newSegmentQueue.bindingKey = "ai.segment.new";
    newSegmentQueue.queueName = "ai.segment.new";
    newSegmentQueue.connectionString = "amqp://guest:guest@rabbit:5672/";

    newSegmentQueueCutTimes.type = QueueConnectionOpt::Direct;
    newSegmentQueueCutTimes.exchange = "ai.segment.new";
    newSegmentQueueCutTimes.bindingKey = "ai.segment.new";
    newSegmentQueueCutTimes.queueName = "ai.segment.cutTimes";
    newSegmentQueueCutTimes.connectionString = "amqp://guest:guest@rabbit:5672/";

    learnedClipQueue.type = QueueConnectionOpt::FanOut;
    learnedClipQueue.exchange = "ai.clip.learned";
    learnedClipQueue.bindingKey = "ai.clip.learned";
    learnedClipQueue.queueName = "";
    learnedClipQueue.connectionString = "amqp://guest:guest@rabbit:5672/";

    rescanClipQueue.type = QueueConnectionOpt::Direct;
    rescanClipQueue.exchange = "ai.clip.new";
    rescanClipQueue.bindingKey = "ai.clip.rescan";
    rescanClipQueue.queueName = "ai.clip.rescan";
    rescanClipQueue.connectionString = "amqp://guest:guest@rabbit:5672/";

    absoluteQueue.type = QueueConnectionOpt::Direct;
    absoluteQueue.exchange = "ai.absolute.exchange";
    absoluteQueue.bindingKey = "ai.absolute.queue";
    absoluteQueue.queueName = "";
    absoluteQueue.connectionString = "amqp://guest:guest@rabbit:5672/";

    absoluteSharedQueue.type = QueueConnectionOpt::Direct;
    absoluteSharedQueue.exchange = "ai.absolute.shared.exchange";
    absoluteSharedQueue.bindingKey = "ai.absolute.shared.queue";
    absoluteSharedQueue.queueName = "ai.absolute.shared.queue";
    absoluteSharedQueue.connectionString = "amqp://guest:guest@rabbit:5672/";

    processedSegmentQueue.type = QueueConnectionOpt::Direct;
    processedSegmentQueue.exchange = "ai.segment.processed";
    processedSegmentQueue.bindingKey = "ai.segment.processed";
    processedSegmentQueue.queueName = "ai.segment.processed";
    processedSegmentQueue.connectionString = "amqp://guest:guest@rabbit:5672/";

    modelDeleteQueue.type = QueueConnectionOpt::FanOut;
    modelDeleteQueue.exchange = "ai.model.deleted";
    modelDeleteQueue.bindingKey = "ai.model.deleted";
    modelDeleteQueue.queueName = "";
    modelDeleteQueue.connectionString = "amqp://guest:guest@rabbit:5672/";
}

void ClipFinderOpt::setEnvironment()
{
    newSegmentQueue.setEnvironment();
    newSegmentQueueCutTimes.setEnvironment();
    learnedClipQueue.setEnvironment();
    rescanClipQueue.setEnvironment();
    processedSegmentQueue.setEnvironment();
    modelDeleteQueue.setEnvironment();
    dbConfig.setEnvironment();

    QString tmp;
    tmp = qgetenv("CFTHREADCOUNT");
    if(!tmp.isEmpty()) recognizerCount = std::max(tmp.toInt(), 1);
}

bool ClipFinderOpt::setJSON(QJsonValue value)
{
    if(!value.isObject()){
        qWarning("Opt: Invalid options - Must be JSON object");
        return false;
    }

    QJsonObject opt = value.toObject();
    //=============================================================== logFile
    if(!getString(opt,logFile,"logFile",false)) return false;
    //=============================================================== logLevel
    if(!getInt(opt,logLevel,"logLevel",false)) return false;
    //=============================================================== maxClipRescanSegments
    if(!getInt(opt,maxClipRescanSegments,"maxClipRescanSegments",false)) return false;
    //=============================================================== newSegmentQueue
    if(!getOpt(opt,newSegmentQueue,"newSegmentQueue",false)) return false;
    //=============================================================== newSegmentQueueCutTimes
    if(!getOpt(opt,newSegmentQueueCutTimes,"newSegmentQueueCutTimes",false)) return false;
    //=============================================================== learnedClipQueue
    if(!getOpt(opt,learnedClipQueue,"learnedClipQueue",false)) return false;
    //=============================================================== rescanClipQueue
    if(!getOpt(opt,rescanClipQueue,"rescanClipQueue",false)) return false;
    //=============================================================== processedSegmentQueue
    if(!getOpt(opt,processedSegmentQueue,"processedSegmentQueue",false)) return false;
    //=============================================================== modelDeleteQueue
    if(!getOpt(opt,modelDeleteQueue,"modelDeleteQueue",false)) return false;
    //=============================================================== adModelConfig
    if(!getOpt(opt,adModelConfig,"adModelConfig",false)) return false;
    //=============================================================== dbConfig
    if(!getOpt(opt,dbConfig,"dbConfig",false)) return false;

    //=============================================================== maxCompFrames
    if(!getInt(opt,maxCompFrames,"maxCompFrames",false)) return false;
    //=============================================================== startFrameCount
    if(!getInt(opt,startFrameCount,"startFrameCount",false)) return false;
    //=============================================================== endFrameCount
    if(!getInt(opt,endFrameCount,"endFrameCount",false)) return false;
    //=============================================================== maxDiff
    if(!getInt(opt,maxDiff,"maxDiff",false)) return false;
    //=============================================================== treshDiff
    if(!getInt(opt,treshDiff,"treshDiff",false)) return false;
    //=============================================================== maxPeakFramesRegion
    if(!getInt(opt,maxPeakFramesRegion,"maxPeakFramesRegion",false)) return false;
    //=============================================================== threshAudioConf
    if(!getInt(opt,threshAudioConf,"threshAudioConf",false)) return false;
    //=============================================================== audioShiftRegion
    if(!getInt(opt,audioShiftRegion,"audioShiftRegion",false)) return false;
    //=============================================================== cutInserterShift
    if(!getInt(opt,cutInserterShift,"cutInserterShift",false)) return false;
    //=============================================================== cutInserterShiftSize
    if(!getInt(opt,cutInserterShiftSize,"cutInserterShiftSize",false)) return false;
    //=============================================================== cutInserterEnableCheck
    if(!getBool(opt,cutInserterEnableCheck,"cutInserterEnableCheck",false)) return false;


    return true;
}

QJsonValue ClipFinderOpt::getJSON()
{
    QJsonObject tObj;
    tObj.insert("logFile",logFile);
    tObj.insert("logLevel",logLevel);
    tObj.insert("maxClipRescanSegments",maxClipRescanSegments);
    tObj.insert("newSegmentQueue",newSegmentQueue.getJSON());
    tObj.insert("newSegmentQueueCutTimes",newSegmentQueueCutTimes.getJSON());
    tObj.insert("learnedClipQueue",learnedClipQueue.getJSON());
    tObj.insert("rescanClipQueue",rescanClipQueue.getJSON());
    tObj.insert("processedSegmentQueue",processedSegmentQueue.getJSON());
    tObj.insert("modelDeleteQueue",modelDeleteQueue.getJSON());
    tObj.insert("adModelConfig",adModelConfig.getJSON());
    tObj.insert("dbConfig",dbConfig.getJSON());

    tObj.insert("maxCompFrames",maxCompFrames);
    tObj.insert("startFrameCount",startFrameCount);
    tObj.insert("endFrameCount",endFrameCount);
    tObj.insert("maxDiff",maxDiff);
    tObj.insert("treshDiff",treshDiff);
    tObj.insert("maxPeakFramesRegion",maxPeakFramesRegion);
    tObj.insert("threshAudioConf",threshAudioConf);
    tObj.insert("audioShiftRegion",audioShiftRegion);
    tObj.insert("cutInserterShift",cutInserterShift);
    tObj.insert("cutInserterShiftSize",cutInserterShiftSize);
    tObj.insert("cutInserterEnableCheck",cutInserterEnableCheck);
    return QJsonValue(tObj);
}

bool ClipFinderOpt::operator==(ClipFinderOpt &other){
    if(logFile != other.logFile) return false;
    if(logLevel != other.logLevel) return false;
    if(newSegmentQueue != other.newSegmentQueue) return false;
    if(newSegmentQueueCutTimes != other.newSegmentQueueCutTimes) return false;
    if(learnedClipQueue != other.learnedClipQueue) return false;
    if(rescanClipQueue != other.rescanClipQueue) return false;
    if(processedSegmentQueue != other.processedSegmentQueue) return false;
    if(modelDeleteQueue != other.modelDeleteQueue) return false;
    if(adModelConfig != other.adModelConfig) return false;
    if(dbConfig != other.dbConfig) return false;

    if(maxClipRescanSegments != other.maxClipRescanSegments) return false;
    if(maxCompFrames != other.maxCompFrames) return false;
    if(startFrameCount != other.startFrameCount) return false;
    if(endFrameCount != other.endFrameCount) return false;
    if(maxDiff != other.maxDiff) return false;
    if(treshDiff != other.treshDiff) return false;
    if(maxPeakFramesRegion != other.maxPeakFramesRegion) return false;
    if(threshAudioConf != other.threshAudioConf) return false;
    if(audioShiftRegion != other.audioShiftRegion) return false;
    if(cutInserterShift != other.cutInserterShift) return false;
    if(cutInserterShiftSize != other.cutInserterShiftSize) return false;
    if(cutInserterEnableCheck != other.cutInserterEnableCheck) return false;
    return true;
}
