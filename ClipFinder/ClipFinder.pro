#-------------------------------------------------
#
# Project created by QtCreator 2015-04-14T14:44:29
#
#-------------------------------------------------

QT       += core network sql

QT       -= gui

TARGET = ClipFinder
CONFIG   += console
CONFIG   -= app_bundle
CONFIG  += c++11

TEMPLATE = app


SOURCES += main.cpp

include(Struct/Struct.pri)
include(Proc/Proc.pri)
include(Opt/Opt.pri)

unix:!macx: LIBS += -L$$OUT_PWD/../SmartDb/ -lSmartDb

INCLUDEPATH += $$PWD/../SmartDb
DEPENDPATH += $$PWD/../SmartDb

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../SmartDb/libSmartDb.a

unix:!macx: LIBS += -L$$OUT_PWD/../SmartOpt/ -lSmartOpt

INCLUDEPATH += $$PWD/../SmartOpt
DEPENDPATH += $$PWD/../SmartOpt

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../SmartOpt/libSmartOpt.a

unix:!macx: LIBS += -L$$OUT_PWD/../SmartQueue/ -lSmartQueue

INCLUDEPATH += $$PWD/../SmartQueue
DEPENDPATH += $$PWD/../SmartQueue

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../SmartQueue/libSmartQueue.a

unix:!macx: LIBS += -L$$OUT_PWD/../SmartQamqp/src/ -lqamqp

INCLUDEPATH += $$PWD/../SmartQamqp/src
DEPENDPATH += $$PWD/../SmartQamqp/src

unix:!macx: LIBS += -L$$OUT_PWD/../SmartModel/ -lSmartModel

INCLUDEPATH += $$PWD/../SmartModel
DEPENDPATH += $$PWD/../SmartModel

unix:!macx: PRE_TARGETDEPS += $$OUT_PWD/../SmartModel/libSmartModel.a

CONFIG += crashdumps

crashdumps {
    DEFINES += CRASHDUMPS
    LIBS += -ldw
    CONFIG += debug
    unix:!macx: LIBS += -L$$OUT_PWD/../SmartCrashlog/ -lSmartCrashlog
    INCLUDEPATH += $$PWD/../SmartCrashlog
    DEPENDPATH += $$PWD/../SmartCrashlog
}
