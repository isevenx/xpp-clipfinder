#ifndef CLIPCACHE_H
#define CLIPCACHE_H

#include <QObject>
#include <QMutex>
#include <QMutexLocker>

#include <map>
#include <memory>

#include "tvmodel.h"

class ClipCache : public QObject
{
    Q_OBJECT
public:
    explicit ClipCache(QObject *parent = 0);
    ~ClipCache();
    typedef struct {
        qint64 clipId; /// same as cutId
        qint64 modelId;
        qint64 specificationId;
        std::shared_ptr<TVModel> model;
    } ClipInstance;


    QVector<ClipInstance> getModels(){QMutexLocker locker(&mapMutex); return models;}

signals:

public slots:
    void pushModel(ClipInstance instance, bool force = false);
    ClipInstance getModel(qint64 modelId, bool &ok);
    void removeModel(qint64 modelId);

private:
    int contains(ClipInstance instance);

    int contains(int modelId);
    QVector<ClipInstance> models;

    mutable QMutex mapMutex;

};

#endif // CLIPCACHE_H
