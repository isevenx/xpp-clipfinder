#ifndef SMARTBUFFER_H
#define SMARTBUFFER_H

#include <QObject>
#include <deque>

#include "qamqpclient.h"
#include "qamqpexchange.h"
#include "qamqpqueue.h"

class SmartBuffer: public QObject
{
    Q_OBJECT

public:
    SmartBuffer(QObject *parent = 0);
    ~SmartBuffer();

    typedef enum {
        newRescanMessage,
        newLearnedMessage,
        newSegmentMessage
    } QueueMessageType;

    struct PendingMessage{
        QAmqpMessage message;
        QueueMessageType type;
    };

    inline int size(){return buffer.size();}

    static QString messageTypeToStr(QueueMessageType type);

public slots:
    void putMessage(QAmqpMessage message, QueueMessageType type);
    PendingMessage getMessage();

private:
    std::deque<PendingMessage> buffer;
};

#endif // SMARTBUFFER_H
