#include "smartbuffer.h"


SmartBuffer::SmartBuffer(QObject *parent):
    QObject(parent)
{

}

SmartBuffer::~SmartBuffer()
{

}

void SmartBuffer::putMessage(QAmqpMessage message, QueueMessageType type)
{
    PendingMessage tMessage;
    tMessage.message = message;
    tMessage.type = type;

    bool inserted = false;
    for(std::deque<PendingMessage>::iterator it = buffer.begin(); it < buffer.end()-1; ++it){
        if(it->type > tMessage.type){
            buffer.insert(it, tMessage);
            inserted = true;
            break;
        }
    }
    if(!inserted){
        buffer.push_back(tMessage);
    }
}

SmartBuffer::PendingMessage SmartBuffer::getMessage()
{
    PendingMessage tMessage;
    if(!buffer.empty()){
        tMessage = buffer[0];
        buffer.pop_front();
    }
    return tMessage;
}

QString SmartBuffer::messageTypeToStr(QueueMessageType type)
{
    QString result;
    switch(type){
        case newRescanMessage: result = "newRescanMessage "; break;
        case newLearnedMessage: result = "newLearnedMessage"; break;
        case newSegmentMessage: result = "newSegmentMessage"; break;
        default: result = "";
    }
    return result;
}
