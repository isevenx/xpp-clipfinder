#include <QObject>
#include <QMutex>
#include <QMutexLocker>

#include <map>
#include <memory>

#include "tvmodel.h"
#include "markupdb.h"

class CutCache : public QObject
{
    Q_OBJECT
public:
    explicit CutCache(QObject *parent = 0);

    std::map<qint64,std::vector<MarkupDB::TimeInfo>> getCutTimes(){QMutexLocker locker(&mapMutex); return cutTimes;}
    std::vector<MarkupDB::TimeInfo> getSegmentTime(qint64 segmentId, bool &ok);
    bool updateCache;
signals:

public slots:
    void removeCutInfo(qint64 segmentId);
    void addCutInfo(qint64 segmentId, std::vector<MarkupDB::TimeInfo> cutInfo);

private:
    std::map<qint64,std::vector<MarkupDB::TimeInfo>> cutTimes;
    mutable QMutex mapMutex;
    std::vector<MarkupDB::TimeInfo> cutTimeInstance;

};


