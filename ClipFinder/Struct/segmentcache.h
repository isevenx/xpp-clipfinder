#ifndef SEGMENTCACHE_H
#define SEGMENTCACHE_H

#include <QObject>
#include <QMutex>
#include <QMutexLocker>
#include <QVector>

#include <map>
#include <memory>

#include "tvmodel.h"
#include "markupdb.h"


class SegmentCache : public QObject
{
    Q_OBJECT
public:
    explicit SegmentCache(QObject *parent = 0);
    ~SegmentCache();

    typedef struct {
        qint64 firstFragment= -1;
        qint64 prevSegmentId = -1;
        qint64 prevSegmentFragment = -1;
        qint64 segmentId;
        qint64 lastUsed;
        qint32 timesUsed;
        std::shared_ptr<TVModel> model;
        bool isUsed = false;
    } ModelInstance;

signals:
    void segmentDeleted(qint64 segmentId);
    void segmentLoaded(qint64 segmentId);

public slots:
    QVector<qint64> cleanup();
    bool pushModel(ModelInstance instance);

    ModelInstance getModel(qint64 segmentId, bool &ok, QString modelname, MarkupDB *markupDb);


    void updateSegmentUse(qint64 segmentId, bool isUsed);

private:

    template <typename T>
    static void FreeAll( T & t ) {
        t.swap( t );
    }
    QVector<ModelInstance> models;
    int contains(ModelInstance  instance);
    int contains(int segmentId);

    std::vector<qint64> segmentIdsVec;


    mutable QMutex mapMutex;

    int maxSegmentsInCache;

};

#endif // SEGMENTCACHE_H
