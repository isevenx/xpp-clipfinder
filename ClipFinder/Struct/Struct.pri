HEADERS += \
    $$PWD/segmentcache.h \
    $$PWD/clipcache.h \
    $$PWD/cutcache.h \
    $$PWD/smartbuffer.h

SOURCES += \
    $$PWD/segmentcache.cpp \
    $$PWD/clipcache.cpp \
    $$PWD/cutcache.cpp \
    $$PWD/smartbuffer.cpp
