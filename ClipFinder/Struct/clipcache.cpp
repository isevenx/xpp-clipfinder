#include "clipcache.h"
#include "smartopt.h"
#include <QDebug>

ClipCache::ClipCache(QObject *parent) :
    QObject(parent)
{
}

ClipCache::~ClipCache()
{
    models.clear();
    models.squeeze();
}

int ClipCache::contains(ClipInstance instance)
{
    return contains(instance.modelId);
}

int ClipCache::contains(int modelId)
{
    for(int it = 0; it< models.size(); it++)
    {
        if(models[it].modelId == modelId) return it;
    }

    return -1;
}

ClipCache::ClipInstance ClipCache::
getModel(qint64 modelId, bool &ok)
{
    QMutexLocker locker(&mapMutex);
    ok = true;

    qint64 it = contains(modelId);
    if (it!=-1) return models[it];
    ok = false;
    return ClipCache::ClipInstance();
}

void ClipCache::pushModel(ClipInstance instance, bool force)
{
    QMutexLocker locker(&mapMutex);

    qint64 it = contains(instance.modelId);
    if( it != -1){
        if(!force) return;
        qDebug() << "Force model update for " << instance.modelId;
        models.erase(models.begin() + it);
    }
    models.push_back(instance);
}

void ClipCache::removeModel(qint64 modelId)
{
    QMutexLocker locker(&mapMutex);

    qint64 it = contains(modelId);
    if(it == -1){
        qDebug() << "ClipCache::removeModel no clip found to erase";
        return;
    }
    models[it].model->clear();
    models.erase(models.begin() + it);
    models.squeeze();
}
