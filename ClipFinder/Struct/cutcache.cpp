#include "cutcache.h"
#include "smartopt.h"
#include <QDebug>

CutCache::CutCache(QObject *parent) :
    QObject(parent)
{
    updateCache = false;
}


std::vector<MarkupDB::TimeInfo> CutCache::getSegmentTime(qint64 segmentId, bool &ok)
{
    QMutexLocker locker(&mapMutex);
    ok = true;
    auto a = cutTimes.find(segmentId);
    if (a == cutTimes.end())
    {
        ok = false;
        return CutCache::cutTimeInstance;
    }
    return a->second;
}

void CutCache::addCutInfo(qint64 segmentId, std::vector<MarkupDB::TimeInfo> cutInfo)
{
    QMutexLocker locker(&mapMutex);
    auto a = cutTimes.find(segmentId);
    if(a != cutTimes.end()) return;
    cutTimes.insert(std::pair<qint64,std::vector<MarkupDB::TimeInfo>>(segmentId,cutInfo));
}


void CutCache::removeCutInfo(qint64 segmentId)
{
    QMutexLocker locker(&mapMutex);

    auto it = cutTimes.find(segmentId);
    if(it == cutTimes.end()){
        qDebug() << "CutCache::removeModel no clip found to erase";
        return;
    }
    cutTimes.erase(it);
}


