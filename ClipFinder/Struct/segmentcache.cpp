#include "segmentcache.h"
#include "smartopt.h"
#include "Opt/clipfinderopt.h"
#include <QDebug>
#include <QDateTime>
#include <algorithm>


SegmentCache::SegmentCache(QObject *parent) :
    QObject(parent)
{
    maxSegmentsInCache = 9;
}

SegmentCache::~SegmentCache()
{
    cleanup();
}

void SegmentCache::updateSegmentUse(qint64 segmentId, bool isUsed)
{
     QMutexLocker locker(&mapMutex);
     int modelNr = contains(segmentId);
     if(modelNr == -1){
        return;
     }

     ModelInstance tInstance = models[modelNr];
     tInstance.isUsed = isUsed;
     models[modelNr] = tInstance;
}


SegmentCache::ModelInstance SegmentCache::getModel(qint64 segmentId, bool &ok, QString modelname, MarkupDB * markupDb)
{
    QMutexLocker locker(&mapMutex);
    SegmentCache::ModelInstance rez;
    int modelNr = contains(segmentId);
    if(modelNr == -1){
        std::vector<MarkupDB::FragmentInfo> segmentFragments = markupDb->getSegmentFragments(segmentId);
        SegmentCache::ModelInstance modelInst;
        if(segmentFragments.empty()){
            ok = false;
            qWarning() << "Failed to find any fragments for video segment in db "<< segmentId;
            return modelInst;
        }

        modelInst.segmentId = segmentId;
        modelInst.lastUsed = QDateTime::currentMSecsSinceEpoch();
        modelInst.timesUsed = 0;
        modelInst.model.reset(new TVModel());

        ok = false;

        QVector<qint64> deletedSegments;
        if (models.size()>maxSegmentsInCache) deletedSegments = cleanup();

        if(modelname == "ad.model") for(auto it = deletedSegments.begin(); it != deletedSegments.end(); it++)
        {
            emit segmentDeleted(*it);
        }

        for(auto it = segmentFragments.begin(); it != segmentFragments.end(); ++it){
            QString filename = QDateTime::fromMSecsSinceEpoch(it->startsAt).toString("%1/hh.mm/%2/yyyy.MM.dd").append("/"+modelname)
                    .arg(SmartOpt<ClipFinderOpt>::getInstance()->adModelConfig.savePath)
                    .arg(it->sourceId);
            ok = modelInst.model->readIntoSelf(filename) || ok;
        }

        if(!ok){
            qWarning() << "Failed to load segment " << segmentId;
            return rez;
        }

        emit segmentLoaded(modelInst.segmentId);
        modelInst.isUsed = true;
        models.push_back(modelInst);
        qDebug() << "SegmentCache current size: " << models.size();

        models.squeeze();
        return modelInst;
    }
    ok = true;

    ModelInstance tInstance = models[modelNr];
    tInstance.lastUsed = QDateTime::currentMSecsSinceEpoch();
    tInstance.timesUsed++;
    tInstance.isUsed = true;
    models[modelNr] = tInstance;

    return tInstance;
}


QVector<qint64> SegmentCache::cleanup()
{
    QVector<qint64> deletedsegments;
    if(models.size() < maxSegmentsInCache) return deletedsegments;

    qDebug() << "SegmentCache::cleanup | Starting cleanup..." << models.size();

    std::vector<qint64> lastUsedTimes;
    for(auto it = models.begin(); it != models.end(); ++it){
        lastUsedTimes.push_back((*it).lastUsed);
    }

    if (lastUsedTimes.size() <= 0) return deletedsegments;

    // Sort last used times descending (because last used ar greatest)
    std::sort(lastUsedTimes.begin(), lastUsedTimes.end(),
        [](const qint64 & a, const qint64 & b) -> bool
    {
        /// (a>b)keeps only maxSegmentsInCache
        /// (a<b)cleans segments to 0
        return a > b;
    });

    // Get treshold from begining of sorted times
    qint64 tresh = lastUsedTimes[std::min((int)(lastUsedTimes.size()-1),maxSegmentsInCache)];

    qDebug() << "lastUsedTimes" << lastUsedTimes.size();
    // Get keys for segments to delete
    qDebug() << "BEFORE cleanup | current size: " << models.size() << "THRESHOLD" << tresh;


    for(auto it = models.begin(); it != models.end();){
        if(it->lastUsed <= tresh){
            deletedsegments.push_back(it->segmentId);
            qDebug() << it->model.use_count() << "removing";
            it->model->clear();
            it->model.reset();
            it = models.erase(it);
        }else{
            ++it;
        }
    }
    models.squeeze();

    qDebug() << "SegmentCache::cleanup | current size: " << models.size();
    return deletedsegments;
}



int SegmentCache::contains(ModelInstance instance)
{
    return contains(instance.segmentId);
}

int SegmentCache::contains(int segmentId)
{
    for(int it = 0; it< models.size(); it++)
    {
        if(models[it].segmentId == segmentId) return it;
    }
    return -1;
}

bool SegmentCache::pushModel(ModelInstance instance)
{
    QMutexLocker locker(&mapMutex);
    if(contains(instance)!=-1) return false;
    models.push_back(instance);
    qDebug() << "SegmentCache::pushModel | current size: " << models.size();

    return true;
}
