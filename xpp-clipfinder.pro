#-------------------------------------------------
#
# Project created by QtCreator 2016-06-29T16:19:55
#
#-------------------------------------------------

TEMPLATE = subdirs
#QMAKE_PKGCONFIG_DESTDIR = pkg-config

CONFIG += ordered

SUBDIRS += \
        SmartCrashlog \
        SmartOpt \
        SmartDb \	
        SmartQamqp \
	SmartQueue \
        SmartModel \
	ClipFinder \
